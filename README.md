# Docker

Docker images used internally (but public).

It looks like CI should build and register a new image in the gitlab registry (?).

For testing:

```
sudo docker build -t full-env:vXX .
sudo docker run -it --rm --network host -v ...:/root/.ssh full-env:vXX
```
