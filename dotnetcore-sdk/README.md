# .NET Core SDK + Dev dependencies

In this image:
* .NET Core SDK 1.1 with MSBuild (not `project.json`)
* NodeJS v7
* Doxygen (latest on apt-get repos)
* Gulp (latest on NPM)
* Bower (latest on NPM)
* Broken Link Checker (latest on NPM)
* HTML Tidy 5.2.0
* Yarn package manager
