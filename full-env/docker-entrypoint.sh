#!/bin/bash --login

# An entrypoint that will run the docker commands within bash.
# This is especially useful for nvm, which is configured via ~/.bashrc.

exec "$@"
