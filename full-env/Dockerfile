FROM ubuntu:22.04

ENTRYPOINT ["/docker-entrypoint.sh"]
COPY docker-entrypoint.sh /

RUN apt-get update && apt-get -y upgrade

# Install GraphViz.
RUN apt-get update && apt-get install -y \
    graphviz \
 && rm -rf /var/lib/apt/lists/*

# Install packages needed to build SandPiper
RUN apt-get update && apt-get install -y \
    libssl-dev \
    build-essential \
    wget \
    curl \
    cmake \
    git \
    python3 \
    default-jdk \
    m4 \
    unzip \
  && rm -rf /var/lib/apt/lists/*

# Download and build Verilator. Does installs to build, then cleans up.
# Verilator dependencies:
RUN apt-get update && apt-get install -y \
    autoconf \
    g++ \
    flex \
    bison \
    python3 \
    help2man \
 && rm -rf /var/lib/apt/lists/*
RUN git clone http://git.veripool.org/git/verilator \
 && cd verilator \
 && git checkout HEAD \
 && autoconf \
 && ./configure \
 && make \
 && make install \
 && cd ../ \
 && rm -rf verilator \
 && apt-get remove -y \
     autoconf \
     flex \
     bison

# Install NVM and node
# NVM modifies ~/.bashrc, but ~/.bashrc has a line that makes it hard to source, so use 'sed' to disable this line.
RUN wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.35.2/install.sh | bash \
 && sed -i 's/\(^.*return\)/#\1/' ~/.bashrc
# Ugh. For some reason NVM installs into /bin/versions/node, but when doing 'docker run', nvm expects /root/.nvm/versions/node ???, so no node installation done.
# && . ~/.nvm/nvm.sh && nvm install 12.0.0 && nvm use 12.0.0 \
# && ls /bin/versions/node && echo $PATH \

## Install npm.
#RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - \
# && apt-get update && apt-get install -y \
#    nodejs \
# && rm -rf /var/lib/apt/lists/*
